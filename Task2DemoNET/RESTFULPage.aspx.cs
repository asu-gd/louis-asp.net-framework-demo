﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Task2DemoNET
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Results_Button_Click(object sender, EventArgs e)
        {
            if(!Session["user"].Equals("User"))
            {
                Response.Redirect("LoginPage.aspx", true);
            }    

            using (var webClient = new System.Net.WebClient())
            {
                string abs = webClient.DownloadString("http://venus.sod.asu.edu/WSRepository/Services/WcfRestService4/Service1/AbsValue?x=" + ABS_Textbox.Text);
                string sum = webClient.DownloadString("http://venus.sod.asu.edu/WSRepository/Services/WcfRestService4/Service1/add2?x=" + Num1_Textbox.Text + "&y=" + Num2_Textbox.Text);
                string pi = webClient.DownloadString("http://venus.sod.asu.edu/WSRepository/Services/WcfRestService4/Service1/PiValue");

                Results_Label.Text = (abs + "       " + sum + "     " + pi);
            }

        }
    }
}