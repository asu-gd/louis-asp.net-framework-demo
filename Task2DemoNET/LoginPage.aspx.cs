﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Runtime.Serialization;
using System.Text;
using System.IO;
using System.Xml;
using System.Data;
using System.Xml.Schema;
using System.Xml.Linq;

namespace Task2DemoNET
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(UsernameTextBox.Text) == false && string.IsNullOrWhiteSpace(UsernameTextBox.Text) == false)
            {
                //Encrypting the username and password
                string username = UsernameTextBox.Text;
                string password = PasswordTextBox.Text;

                //Loading the Users.xml doc
                XmlDocument doc = new XmlDocument();
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + "Users.xml");
                var nodes = doc.GetElementsByTagName("User");
                var resultNodes = new List<XmlNode>();
                bool validUser = false;

                //Searching the Members.xml for a user element that has the correct username and password
                foreach (XmlNode node in nodes)
                {
                    if (node.Attributes != null && node.Attributes["Username"] != null && node.Attributes["Username"].Value == username
                                                && node.Attributes["Password"] != null && node.Attributes["Password"].Value == password)
                    {
                        validUser = true;
                        break;
                    }
                }

                //Decision depending on if its a valid user or not
                if (validUser)
                {
                    IncorrectLoginLabel.Text = "";
                    Session["user"] = "User";
                    Response.Redirect("RESTFULPage.aspx", true);
                }

                else
                {
                    IncorrectLoginLabel.Text = "Incorrect username or password!";
                }
            }

            else
            {
                IncorrectLoginLabel.Text = "Incorrect username or password!";
            }
        }
    }
}