﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RESTFULPage.aspx.cs" Inherits="Task2DemoNET.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Absolute Value"></asp:Label>
            <br />
&nbsp;<asp:TextBox ID="ABS_Textbox" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="Add Two Numbers"></asp:Label>
            <br />
            <br />
            <asp:TextBox ID="Num1_Textbox" runat="server"></asp:TextBox>
&nbsp;<br />
            <asp:TextBox ID="Num2_Textbox" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" Text="Get Pi"></asp:Label>
            <br />
            <br />
            <asp:Button ID="Results_Button" runat="server" OnClick="Results_Button_Click" Text="Button" />
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" Text="Results:"></asp:Label>
            <br />
            <asp:Label ID="Results_Label" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
